Author: Alex Vischer
email: avischer@uoregon.edu

Here we have continued to modify the brevets web app that tells you the various controle locations with numerous built-in APIs; some can query the database for earlist controle open and closing times.

You can find the flask app that uses Mongodb at port 5000, while the consumer product is located at port 5001.

In order to use the consumer product, as it involves cross-domain requests, you need to install the chrome extension located here:
https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi/related?hl=en-US

The consumer page should simply display the current brevet open and closing times. 
